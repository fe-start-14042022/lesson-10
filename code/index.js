//Створення елементів 
function createElement (elementName, className = "", content = "", link = "") {
    const element = document.createElement(elementName);
    if(className !== "" || className !== null){
        element.className = className;
    }
    if(content !== "" || content !== null){
        element.textContent = content;
    }
    if(link !== "" || link !== null){
        element.href = link;
    }
    return element
}

const navItems = ["Головна", "продукти", "послуги", "про нас", "контакти"];


// Збереження / додавання
const header = createElement("header", "header-top"),
logo = createElement("img", "header-top__logo"),
nav = createElement("ul", "nav"), 
lis = navItems.map(function (el) {
    const li = createElement("li", "nav-items")
    const a = createElement("a", undefined, el, "#")
    li.append(a);
    return li;
}), 
container = createElement("div", "container")
containerChild = [
createElement("div"),
createElement("div"),
createElement("div"),
createElement("div")
],
leftDiv = createElement("div", "left-div"),
rightDiv = createElement("div", "right-div");



nav.append(...lis)
header.prepend(logo);
header.append(nav);
container.append(...containerChild)
containerChild[0].className = "left-elements"
containerChild[0].append(leftDiv)
containerChild[3].className = "right-elements"
containerChild[3].append(rightDiv)


logo.src = "./img/logo.svg"





window.onload = () => {
    document.body.prepend(container);
    document.body.prepend(header);
  
}
